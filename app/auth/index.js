/**
 * Authentication Routes
 */

'use strict';

var router = require('express').Router(),
    User   = require('../users/user.model');

require('./local/passport').setup(User);

router.use('/local', require('./local'));

module.exports = router;
