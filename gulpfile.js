var gulp    = require('gulp');
var nodemon = require('gulp-nodemon');
var jshint  = require('gulp-jshint');

var paths = {
  server: {
    start: 'app/app.js',
    scripts: ['app/*.js', 'app/**/*.js']
  }
};

// lint the server
gulp.task('lint:server', function() {
  return gulp.src(paths.server.scripts)
    .pipe(jshint({ node: true }))
    .pipe(jshint.reporter('jshint-stylish'));
});

// start and watch the server
gulp.task('server:run', function() {
  nodemon({
    script: paths.server.start,
    env: { NODE_ENV: 'development' }
  }).on('change', ['server:restart']);
});
gulp.task('server:restart', ['lint:server']);

// Gulp Actions
gulp.task('default', ['lint:server', 'server:run']);
